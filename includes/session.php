<?php

  class Session {
    private $logged_in = false;
    private $allowedSchool = "School of Computing, Electronics and Mathematics (Faculty of Science and Engineering)";
    public $user_id;
    public $user_cardNo;
    public $user_name;
    public $user_mail;
    public $user_school;
    public $user_modules;

    function __construct(){
      session_start();
      $this->check_login();
      if($this->logged_in){
        // do action if logged in
      } else {
        // do action if not logged in
      }
    }

    public function is_logged_in(){
      return $this->logged_in;
    }

    function is_finalYear($modules) {
      $isTrue = false;
      $moduleArray = explode(" ",$modules);
      foreach ($moduleArray as $module) {
        $intStr = filter_var($module, FILTER_SANITIZE_NUMBER_INT);
        $valArr = str_split($intStr);
        if($valArr[0] >= 3){
          $isTrue = true;
        }
      }
      return $isTrue;
    }

    function uni_login($fullName, $email, $school, $modules, $id, $cardNo, $finalYear) {
      if($school === $this->allowedSchool && $finalYear === true) {
        $this->user_id = $_SESSION['user_id'] = $id;
        $this->user_cardNo = $_SESSION['user_cardNo'] = $cardNo;
        $this->user_name = $_SESSION['user_name'] = $fullName;
        $this->user_mail = $_SESSION['user_mail'] = $email;
        $this->user_school = $_SESSION['user_school'] = $school;
        $this->user_modules = $_SESSION['user_modules'] = $modules;
        $this->logged_in = true;
        return true;
      } else {
        return false;
      }
    }

    function admin_login($fullName, $email, $school, $id, $role) {
      if($school === $this->allowedSchool && $role == "FT-Staff") {
        $this->user_id = $_SESSION['user_id'] = $id;
        $this->user_name = $_SESSION['user_name'] = $fullName;
        $this->user_mail = $_SESSION['user_mail'] = $email;
        $this->logged_in = true;
        return true;
      } else {
        return false;
      }
    }

    public function logout(){
      unset($_SESSION['user_id']);
      unset($this->user_id);
      unset($_SESSION['user_name']);
      unset($this->user_name);
      unset($_SESSION['user_mail']);
      unset($this->user_mail);
      $this->logged_in = false;
    }

    private function check_login(){
      if(isset($_SESSION['user_id'])){
        $this->user_id = $_SESSION['user_id'];
        $this->logged_in = true;
      } else {
        unset($this->user_id);
        $this->logged_in = false;
      }
    }

  }
  $session = new Session();
?>
