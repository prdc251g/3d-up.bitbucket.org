<?php
  require_once("config.php");

  class MySQLDatabase {
    private $connection;

    function __construct(){
      $this->open_connection();
    }

    public function open_connection(){
      // Create a connection to the database
      $this->connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
      if(mysqli_connect_errno()){
        die("Database connection failed: " .
            mysqli_connect_error() .
            " (" . mysqli_connect_errno() . ")"
          );
      }
    }
    public function close_connection(){
      // Close connection
      if(isset($this->connection)){
        mysqli_close($this->connection);
        unset($this->connection);
      }
    }
    public function query($sql){
      $result = mysqli_query($this->connection, $sql);
      $this->confirm_query($result, $sql);
      return $result;
    }
    private function confirm_query($result, $sql){
      if(!$result){
        return false;
      }
    }
    public function escape_value($string){
      $escaped_string = mysqli_real_escape_string($this->connection, $string);
      return $escaped_string;
    }
    public function find_price($name){
      $result = $this->query("SELECT cost FROM materials WHERE name='{$name}' LIMIT 1");
      $row = $this->fetch_row($result);
      return $row[0];
    }
    public function find_last_id($table_name=""){
      $result = $this->query("SELECT id FROM ". $table_name ." ORDER BY id DESC LIMIT 1");
      $row = $this->fetch_row($result);
      return $row[0];
    }
    public function delete_by_id($id, $table_name=""){
      $this->query("DELETE FROM ".$table_name." WHERE id={$id} LIMIT 1");
      return ($this->affected_rows() == 1) ? true : false;
    }

    // Database neutral functions
    public function num_fields($result_set){
      return mysql_num_fields($result_set);
    }
    public function fetch_field($result_set){
      return mysqli_fetch_field($result_set);
    }
    public function fetch_row($result_set){
      return mysqli_fetch_row($result_set);
    }
    public function fetch_array($result_set){
      return mysqli_fetch_array($result_set);
    }
    public function num_rows($result_set){
      return mysqli_num_rows($result_set);
    }
    public function insert_id(){
      return mysqli_insert_id($this->connection);
    }
    public function affected_rows(){
      return mysqli_affected_rows($this->connection);
    }
  }

  $database = new MySQLDatabase();
  $db =& $database;

?>
