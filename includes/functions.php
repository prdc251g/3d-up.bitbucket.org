<?php

  function redirect_to($location = NULL){
    if($location != NULL) {
      header("Location: {$location}");
      exit;
    }
  }

  function output_message($message=""){
    if(!empty($message)){
      return "<p class=\"message\">{$message}</p>";
    } else {
      return "";
    }
  }

  function form_errors($errors=array()) {
    $output = "";
    if (!empty($errors)) {
      $output .= "<div class=\"error\">";
      $output .= "Please fix the following errors: ";
      $output .= "<ul>";
      foreach ($errors as $key => $error) {
        $output .= "<li>";
        $output .= htmlentities($error);
        $output .= "</li>";
      }
      $output .= "</ul>";
      $output .= "</div>";
    }
    return $output;
  }

  function __autoload($class_name){
    $class_name = strtolower($class_name);
    $path = "../includes/{$class_name}.php";
    if(file_exists($path)){
      require_once($path);
    } else {
      die("The file {$class_name}.php could not be found");
    }
  }

  function include_layout_template($template=""){
    include("../public/layouts/{$template}");
  }

  function include_admin_layout_template($template=""){
    include("../../public/layouts/{$template}");
  }

?>
