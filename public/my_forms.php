<?php
  require_once("../includes/initialize.php");
  if(!$session->is_logged_in()){
    redirect_to("login.php");
  }
?>

<?php include_layout_template('header.php') ?>
      <div id="page">
        <h2>My Form Table</h2>
        <br/>
        <table width="100%" border="1" cellpadding="1" cellspacing="1">
          <tr id="sticky">
            <th>Timestamp</th>
            <th>Full Name</th>
            <th>E-Mail</th>
            <th>Module Code</th>
            <th>Comments</th>
            <th>Print Date</th>
            <th>Print Time</th>
            <th>Printer</th>
            <th>Instructions</th>
            <th>Cost</th>
            <th>Status</th>
          </tr>
          <?php
            global $db;
            $user_id = $_SESSION['user_id'];
            $records = $db->query("SELECT * FROM forms WHERE user_id={$user_id}");
            while($object = mysqli_fetch_assoc($records)) {
              echo "<tr>";
              echo "<td>".$object['time_stamp']."</td>";
              echo "<td>".$object['full_name']."</td>";
              echo "<td>".$object['email']."</td>";
              echo "<td>".$object['module_code']."</td>";
              echo "<td>".$object['comments']."</td>";
              if ($object['print_date'] != ""){
                echo "<td>".$object['print_date']."</td>";
              } else {
                echo "<td>Pending</td>";;
              }
              if ($object['print_time'] != ""){
                echo "<td>".$object['print_time']."</td>";
              } else {
                echo "<td>Pending</td>";;
              }
              echo "<td>".$object['printer']."</td>";
              echo "<td>".$object['instructions']."</td>";
              if ($object['cost'] != ""){
                echo "<td>".$object['cost']."</td>";
              } else {
                echo "<td>Pending</td>";;
              }
              echo "<td>".$object['status']."</td>";
              echo "</tr>";
            }
          ?>
        </table>
      </div>
    </div>
<?php include_layout_template('footer.php') ?>
