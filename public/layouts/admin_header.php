<html>
  <head>
    <title>3D Print Facility: Admin</title>
    <link href="../stylesheets/style.css" media="all" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <div id="header">
      <div id="himg"><img src="../assets/plym2d.png"></div>
      <div id="hh1"><h1>3D Print Submission System: Admin</h1></div>
    </div>
    <div id="main">
      <div id="navigation">
    		<div id="nav">
    		<h4>Form Actions</h4>
    		<ul>
    			<li><a href="pending_forms.php">Pending Forms</a></li>
    			<li><a href="meeting_forms.php">Meeting Forms</a></li>
    			<li><a href="form_table.php">All Forms</a></li>
    		</ul>

    		<h4>Table Actions</h4>
    		<ul>
    			<li><a href="material_table.php">Material Records</a></li>
          <li><a href="quality_table.php">Quality Records</a></li>
          <li><a href="colour_table.php">Colour Records</a></li>
    			<li><a href="printer_table.php">Printer Records</a></li>
    			<li><a href="object_table.php">Object Records</a></li>
    		</ul>

    		<h4>Main</h4>
    		<ul>
          <li><a href="invoice.php">Export Invoice</a></li>
    			<li><a href="index.php">Show Main</a></li>
    			<li><a href="logout.php">Log Out</a>
    		</ul>
    		</div>
    		<div id="line"></div>
      </div>
