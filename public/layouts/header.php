<html>
  <head>
    <title>3D Print Facility</title>
    <link href="stylesheets/style.css" media="all" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <div id="header">
      <div id="himg"><img src="assets/plym2d.png"></div>
      <div id="hh1"><h1>3D Print Submission System</h1></div>
    </div>
    <div id="main">
      <div id="navigation">
    		<div id="nav">
          <div id="nav-item">
      			<a class="ad" href="index.php">Main Menu</a>
      		</div>
      		<div id="nav-item">
      			<a class="ad" href="request_form.php">Request Form</a>
      		</div>
      		<div id="nav-item">
      			<a class="ad" href="my_forms.php">My Forms</a>
      		</div>
      		<div id="nav-item">
      			<a class="ad" href="logout.php">Log Out</a>
      		</div>
        </div>
        <div id="line"></div>
      </div>
