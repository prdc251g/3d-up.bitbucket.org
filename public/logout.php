<?php
  require_once("../includes/initialize.php");
  if(!$session->is_logged_in()){
    redirect_to("login.php");
  }
?>
<?php include_layout_template('header.php') ?>
  <meta http-equiv="refresh" content="0;URL=index.php" />
    <div id="main">
      <h2>Log User Out</h2>
      <?php
        $session->logout();
        exit();
        redirect_to("login.php");
      ?>
    </div>

<?php include_layout_template('footer.php') ?>
