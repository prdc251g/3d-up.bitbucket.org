<?php
  require_once("../../includes/initialize.php");
  if(!$session->is_logged_in()){ redirect_to("login.php"); }
  global $db;

  $id = $_GET['id'];
  $table_name = $_GET['table'];
  $link = $_GET['link'];
  $try = $db->delete_by_id($id, $table_name);

  if($try) {
    redirect_to($link . ".php?d=success&id=".$id);
  } else {
    die("Failed to delete record");
  }
?>
