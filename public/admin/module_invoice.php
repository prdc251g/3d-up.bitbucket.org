<?php
  require_once("../../includes/initialize.php");
  global $db;

  if(isset($_POST['submit'])) {

    $required_fields = array("module_code");
    validate_presence($required_fields);

    if (empty($errors)) {

      $module_code = $db->escape_value($_POST["module_code"]);

      $moduleInfo = $db->query("SELECT id FROM forms WHERE module_code='{$module_code}'");

      $module = mysqli_fetch_assoc($moduleInfo);

      if ($module['id'] != "") {

        $moduleCode = "Module Code: " ."\t". $module_code;

        $join = $db->query("SELECT forms.id, forms.user_id, forms.full_name, objects.material,  objects.quality,  forms.printer, forms.status, objects.cost FROM forms LEFT JOIN objects ON forms.id = objects.id WHERE forms.module_code = '{$module_code}'");

        $columnNames = "Submissions ID" . "\t" . "Student ID" . "\t" . "Full Name" . "\t" . "Material" . "\t" . "Quality" . "\t" . "Printer" . "\t" . "Status" . "\t" . "Cost" . "\t";
        $totalCost = "" . "\t" . "" . "\t" . "" . "\t" . "" . "\t" ."" . "\t" . "" . "\t" . "Total Cost" . "\t";

        $setData = '';

        while ($rec = mysqli_fetch_row($join)) {
            $rowData = '';
            foreach ($rec as $value) {
                $value = '"' . $value . '"' . "\t";
                $rowData .= $value;
            }
            $setData .= trim($rowData) . "\n";
        }

        header("Content-type: application/xls");
        header("Content-Disposition: attachment; filename=".$module_code."-invoice.xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo $moduleCode . "\n \n" . $columnNames . "\n" . $setData . "\n" . $totalCost . "\n";
      } else {
        redirect_to("invoice.php?module_message=The Module Code entered does not exist");
      }
    } else {
      redirect_to("invoice.php?module_message=Module Code fieldname can't be blank");
    }
  }
?>
