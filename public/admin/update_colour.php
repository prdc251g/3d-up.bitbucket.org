<?php
  require_once("../../includes/initialize.php");
  if(!$session->is_logged_in()){ redirect_to("login.php"); }
  $message = "";
?>

<?php
  if (isset($_POST['submit'])) {
    // Process the form
    global $db;

    if (empty($errors)) {
      $id = $db->escape_value($_POST["id"]);
      $name = $db->escape_value($_POST["name"]);

      $sql = "UPDATE colours ";
      $sql .= " SET name='{$name}' ";
      $sql .= " WHERE id={$id}";
      $result = $db->query($sql);

      if ($result) {
        $_SESSION["message"] = "Colour updated.";
        redirect_to("colour_table.php");
      } else {
        $_SESSION["message"] = "Colour update failed.";
      }
    }
  }
?>

<?php include_admin_layout_template('admin_header.php') ?>
      <div id="page">
        <h2>Update Colour</h2>
        <?php echo output_message($message); ?>

        <form action="update_colour.php" method="post">
          <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" />
          <p>Colour Name: <input type="text" name="name" value="" /> </p>
          <input type="submit" name="submit" value="Update Colour" />
        </form>
      </div>
    </div>
    <div id="footer">
      Copyright <?php echo date("Y"); ?>, G Team
    </div>
  </body>
</html>
