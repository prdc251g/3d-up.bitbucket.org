<?php
  require_once("../../includes/initialize.php");
  if(!$session->is_logged_in()){
    redirect_to("login.php");
  }
  if(isset($_GET['d'])){
    $d = $_GET['d'];
    $oid = $_GET['id'];
  } else {
    $d = "";
  }
  if($d == "") {
    $message = "";
  } else {
    $message = "<span style=\"color:red\">Object with ID=".$oid." has been successfully deleted!</span>";
  }
?>

<?php include_admin_layout_template('admin_header.php') ?>
      <div id="page">
        <form id="search" action="object_table.php" method="get">
          <h4>Search the Table</h4>
          <p>Search by: <select name="search_option">
                        <?php
                          $search_options = ["ID" => "id", "Student ID" => "user_id", "Filename" => "filename", "Quantity" => "quantity", "Dimension X" => "dim_x", "Dimension Y" => "dim_y", "Dimension Z" => "dim_z", "Material" => "material", "Quality" => "quality", "Weight" => "weight", "Comments" => "comments", "Cost" => "cost" ];
                          foreach ($search_options as $key => $value) {
                            echo "<option value='".$value."'>".$key."</option>";
                          }
                        ?>
                      </select>
            value: <input type="text" name="search_value" value="" />
          <input type="submit" name="submit" value="search" /></p>
        </form>
      </br></br>
        <h2>Object Table</h2></br>
        <?php echo output_message($message); ?></br>
        <table width="100%" border="1" cellpadding="1" cellspacing="1">
          <tr id="sticky">
            <th>ID</th>
            <th>Student ID</th>
            <th>Filename</th>
            <th>Quantity</th>
            <th>Dimension X</th>
            <th>Dimension Y</th>
            <th>Dimension Z</th>
            <th>Material</th>
            <th>Quality</th>
            <th>Weight in Grams</th>
            <th>Cost £/g</th>
            <th>Comments</th>
            <th>Delete</th>
          </tr>
          <?php
            global $db;
            if(isset($_GET['submit'])){
              $state = $db->escape_value($_GET['submit']);
              $option = $db->escape_value($_GET['search_option']);
              $value = $db->escape_value($_GET['search_value']);
            } else {
              $state = "";
            }

            if($state == "search") {
              $records = $db->query("SELECT * FROM objects WHERE $option LIKE '%$value%'");
            } else {
              $records = $db->query("SELECT * FROM objects");
            }

            while($object = mysqli_fetch_assoc($records)) {
              echo "<tr>";
              echo "<td>".$object['id']."</td>";
              echo "<td>".$object['user_id']."</td>";
              echo "<td>".$object['filename']."</td>";
              echo "<td>".$object['quantity']."</td>";
              echo "<td>".$object['dim_x']."</td>";
              echo "<td>".$object['dim_y']."</td>";
              echo "<td>".$object['dim_z']."</td>";
              echo "<td>".$object['material']."</td>";
              echo "<td>".$object['quality']."</td>";
              if ($object['weight'] != ""){
                echo "<td>".$object['weight']."</td>";
              } else {
                echo "<td><a href=\"update_object.php?id=".$object['id']."&material=".$object['material']."\">
                        <img src=\"../assets/edit.png\" alt=\"Edit button\" style=\"width:30px;height:30px;border:0;\">
                      </a></td>";;
              }
              if ($object['cost'] != ""){
                echo "<td>".$object['cost']."</td>";
              } else {
                echo "<td>Pending</td>";
              }
              echo "<td>".$object['comments']."</td>";
              echo "<td><a href=\"delete_record.php?id=".$object['id']."&table=objects&link=object_table\">
                      <img src=\"../assets/trash.png\" alt=\"Delete button\" style=\"width:30px;height:30px;border:0;\">
                    </a></td>";
              echo "</tr>";
            }
          ?>
        </table>
      </div>
    </br></br>
    </div>

<?php include_admin_layout_template('footer.php') ?>
