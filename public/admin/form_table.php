<?php
  require_once("../../includes/initialize.php");
  if(!$session->is_logged_in()){
    redirect_to("login.php");
  }
  if(isset($_GET['d'])){
    $d = $_GET['d'];
    $oid = $_GET['id'];
  } else {
    $d = "";
  }
  if($d == "") {
    $message = "";
  } else {
    $message = "<span style=\"color:red\">Form with ID=".$oid." has been successfully deleted!</span>";
  }
?>

<?php include_admin_layout_template('admin_header.php') ?>
      <div id="page">
        <form id="search" action="form_table.php" method="get">
          <h4>Search the Table</h4>
          <p>Search by: <select name="search_option">
                        <?php
                          $search_options = ["ID" => "id", "Timestamp" => "time_stamp", "Card Number" => "card_number", "Full Name" => "full_name", "Student ID" => "user_id", "E-Mail" => "email", "Module Code" => "module_code", "Comments" => "comments", "Print Date" => "print_date", "Printer" => "printer", "Instructions" => "instructions", "Cost" => "cost", "Status" => "status", "Updated By" => "updated_by" ];
                          foreach ($search_options as $key => $value) {
                            echo "<option value='".$value."'>".$key."</option>";
                          }
                        ?>
                      </select>
            value: <input type="text" name="search_value" value="" />
          <input type="submit" name="submit" value="search" /></p>
        </form>
      </br></br>
        <h2>Form Table</h2></br>
        <?php echo output_message($message); ?></br>
        <table width="100%" border="1" cellpadding="1" cellspacing="1">
          <tr id="sticky">
            <th>ID</th>
            <th>Timestamp</th>
            <th>Card Number</th>
            <th>Full Name</th>
            <th>Student ID</th>
            <th>E-Mail</th>
            <th>Module Code</th>
            <th>Comments</th>
            <th>Print Date</th>
            <th>Print Time</th>
            <th>Printer</th>
            <th>Instructions</th>
            <th>Cost</th>
            <th>Status</th>
            <th>Delete</th>
          </tr>
          <?php
            global $db;
            if(isset($_GET['submit'])){
              $state = $db->escape_value($_GET['submit']);
              $option = $db->escape_value($_GET['search_option']);
              $value = $db->escape_value($_GET['search_value']);
            } else {
              $state = "";
            }

            if($state == "search") {
              $records = $db->query("SELECT * FROM forms WHERE $option LIKE '%$value%'");
            } else {
              $records = $db->query("SELECT * FROM forms");
            }

            while($object = mysqli_fetch_assoc($records)) {
              echo "<tr>";
              echo "<td>".$object['id']."</td>";
              echo "<td>".$object['time_stamp']."</td>";
              echo "<td>".$object['card_number']."</td>";
              echo "<td>".$object['full_name']."</td>";
              echo "<td>".$object['user_id']."</td>";
              echo "<td>".substr($object['email'], 0, -15)."</td>";
              echo "<td>".$object['module_code']."</td>";
              echo "<td>".$object['comments']."</td>";
              if ($object['print_date'] != ""){
                echo "<td>".$object['print_date']."</td>";
              } else {
                echo "<td>Pending</td>";
              }
              if ($object['print_time'] != ""){
                echo "<td>".$object['print_time']."</td>";
              } else {
                echo "<td>Pending</td>";
              }
              echo "<td>".$object['printer']."</td>";
              echo "<td>".$object['instructions']."</td>";
              if ($object['cost'] != ""){
                echo "<td>".$object['cost']."</td>";
              } else {
                echo "<td>Pending</td>";
              }
              if ($object['status'] == "Pending"){
                echo "<td><a href=\"answer_form.php?id=".$object['id']."&name=".$object['full_name']."&mail=".$object['email']."\">
                        <img src=\"../assets/edit.png\" alt=\"Edit button\" style=\"width:30px;height:30px;border:0;\">
                      </a></td>";
              } else {
                echo "<td>".$object['status']." by ".$object['updated_by']."</td>";
              }
              echo "<td>
                        <a href=\"delete_record.php?id=".$object['id']."&table=forms&link=form_table\">
                      <img src=\"../assets/trash.png\" alt=\"Delete button\" style=\"width:30px;height:30px;border:0;\">
                    </a></td>";
              echo "</tr>";
            }
          ?>
        </table>
      </div>
    </br></br>
    </div>

<?php include_admin_layout_template('footer.php') ?>
