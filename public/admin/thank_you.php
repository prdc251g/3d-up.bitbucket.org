<?php
  require_once("../../includes/initialize.php");
  if(!$session->is_logged_in()){ redirect_to("login.php"); }
?>

<?php include_admin_layout_template('admin_header.php') ?>
    <div id="page">
      <h2>You have successfully updated the form for the print request!</h2>
      <br/>
      <p>We have sent you a confirmation email to the students university email address with the details of the request forms status.</p>
      <br/>
      <a href="pending_forms.php">Have more forms to review?</a>
    </div>
  </div>
  <div id="footer">
    Copyright <?php echo date("Y"); ?>, G Team
  </div>
</body>
</html>
