<?php
  require_once("../../includes/initialize.php");
  if(!$session->is_logged_in()){
    redirect_to("login.php");
  }
  if(isset($_GET['d'])){
    $d = $_GET['d'];
    $oid = $_GET['id'];
  } else {
    $d = "";
  }
  if($d == "") {
    $message = "";
  } else {
    $message = "<span style=\"color:red\">Quality with ID=".$oid." has been successfully deleted!</span>";
  }
?>

<?php
  if (isset($_POST['submit'])) {
    // Process the form
    global $db;

    $required_fields = array("quality", "layer", "infill", "wall");
    validate_presence($required_fields);

    $fields_with_max_lengths = array("quality" => 15, "layer" => 15, "infill" => 4, "wall" => 25);
    validate_max_lengths($fields_with_max_lengths);

    if (empty($errors)) {
      $quality = $db->escape_value($_POST["quality"]);
      $layer_height = $db->escape_value($_POST["layer"]);
      $infill_density = $db->escape_value($_POST["infill"]);
      $wall_thickness = $db->escape_value($_POST["wall"]);

      $sql = "INSERT INTO quality (";
      $sql .= " quality, layer_height, infill_density, wall_thickness";
      $sql .= ") VALUES (";
      $sql .= " '{$quality}', '{$layer_height}', '{$infill_density}', '{$wall_thickness}'";
      $sql .= ")";
      $result = $db->query($sql);

      if ($result) {
        $_SESSION["message"] = "Quality created.";
        redirect_to("quality_table.php");
      } else {
        $_SESSION["message"] = "Quality creation failed.";
      }
    } else {
      $message = form_errors($errors);
    }
  }
?>

<?php include_admin_layout_template('admin_header.php') ?>
      <div id="page">
        <h2>Add a new Quality Level</h2>
        <?php echo output_message($message); ?>
        <form action="quality_table.php" method="post">
          <p>Quality level: <input type="text" name="quality" value="" /> </p>
          <p>Layer height: <input type="text" name="layer" value="" /> </p>
          <p>Infill density: <input type="text" name="infill" value="" /> </p>
          <p>Wall thickness: <input type="text" name="wall" value="" /> </p>
          <input type="submit" name="submit" value="Add Quality" />
        </form>
      </br></br>
        <h2>Quality Table</h2></br>
        <table width="100%" border="1" cellpadding="1" cellspacing="1">
          <tr id="sticky">
            <th>ID</th>
            <th>Quality Level</th>
            <th>Layer Height</th>
            <th>Infill Density</th>
            <th>Wall Thickness</th>
            <th>Edit</th>
          </tr>
          <?php
            global $db;
            $records = $db->query("SELECT * FROM quality");
            while($object = mysqli_fetch_assoc($records)) {
              echo "<tr>";
              echo "<td>".$object['id']."</td>";
              echo "<td>".$object['quality']."</td>";
              echo "<td>".$object['layer_height']."</td>";
              echo "<td>".$object['infill_density']."</td>";
              echo "<td>".$object['wall_thickness']."</td>";
              echo "<td><a href=\"update_quality.php?id=".$object['id']."\">
                      <img src=\"../assets/edit.png\" alt=\"Edit button\" style=\"width:30px;height:30px;border:0;\">
                    </a>
                        <a href=\"delete_record.php?id=".$object['id']."&table=printers\">
                      <img src=\"../assets/trash.png\" alt=\"Delte button\" style=\"width:30px;height:30px;border:0;\">
                    </a></td>";
              echo "</tr>";
            }
          ?>
        </table>
      </div>
    </div>
    <div id="footer">
      Copyright <?php echo date("Y"); ?>, G Team
    </div>
  </body>
</html>
