<?php
  require_once("../../includes/initialize.php");
  if(!$session->is_logged_in()){
    redirect_to("login.php");
  }
  if(isset($_GET['d'])){
    $d = $_GET['d'];
    $oid = $_GET['id'];
  } else {
    $d = "";
  }

  if($d == "") {
    $message = "";
  } else {
    $message = "<span style=\"color:red\">Material with ID=".$oid." has been successfully deleted!</span>";
  }
?>

<?php
  if (isset($_POST['submit'])) {
    // Process the form
    global $db;

    $required_fields = array("name", "cost");
    validate_presence($required_fields);

    $fields_with_max_lengths = array("name" => 20);
    validate_max_lengths($fields_with_max_lengths);

    if (empty($errors)) {
      $name = $db->escape_value($_POST["name"]);
      $cost = $db->escape_value($_POST["cost"]);

      $sql = "INSERT INTO materials (";
      $sql .= " name, cost";
      $sql .= ") VALUES (";
      $sql .= " '{$name}', {$cost}";
      $sql .= ")";
      $result = $db->query($sql);

      if ($result) {
        redirect_to("material_table.php");
      } else {
        $message = "<span style=\"color:red\">Material creation failed. Please make sure that the cost field is numerals only.</span>";
      }
    } else {
      $message = form_errors($errors);
    }
  }
?>

<?php include_admin_layout_template('admin_header.php') ?>
      <div id="page">
        <h2>Add a new Material</h2>
        <?php echo output_message($message); ?>
        <form action="material_table.php" method="post">
          <p>Material: <input type="text" name="name" value="" /> </p>
          <p>Cost £/g: <input type="text" name="cost" value="" /> </p>
          <input type="submit" name="submit" value="Add Material" />
        </form>
      </br></br>
        <h2>Material Table</h2><br/>
        <table width="100%" border="1" cellpadding="1" cellspacing="1">
          <tr id="sticky">
            <th>ID</th>
            <th>Name</th>
            <th>Cost £/g</th>
            <th>Edit</th>
          </tr>
          <?php
            global $db;
            $records = $db->query("SELECT * FROM materials");
            while($object = mysqli_fetch_assoc($records)) {
              echo "<tr>";
              echo "<td>".$object['id']."</td>";
              echo "<td>".$object['name']."</td>";
              echo "<td>".$object['cost']."</td>";
              echo "<td><a href=\"update_material.php?id=".$object['id']."\">
                      <img src=\"../assets/edit.png\" alt=\"Edit button\" style=\"width:30px;height:30px;border:0;\">
                    </a><a href=\"delete_record.php?id=".$object['id']."&table=materials&link=material_table\">
                      <img src=\"../assets/trash.png\" alt=\"Delete button\" style=\"width:30px;height:30px;border:0;\">
                    </a></td>";
              echo "</tr>";
            }
          ?>
        </table>
      </div>
    </div>
    <div id="footer">
      Copyright <?php echo date("Y"); ?>, G Team
    </div>
  </body>
</html>
