<?php
  require_once("../../includes/initialize.php");
  if(!$session->is_logged_in()){ redirect_to("login.php"); }
  $message = "";
?>

<?php
  if (isset($_POST['submit'])) {
    // Process the form
    global $db;

    $required_fields = array("weight");
    validate_presence($required_fields);

    if (empty($errors)) {
      $id = $db->escape_value($_POST["id"]);
      $weight = $db->escape_value($_POST["weight"]);
      $cost = $db->escape_value($_POST["cost"]);

      $sql = "UPDATE objects ";
      $sql .= " SET weight={$weight}, cost={$cost} ";
      $sql .= " WHERE id={$id}";
      $result = $db->query($sql);

      $sql2 = "UPDATE forms ";
      $sql2 .= " SET cost={$cost} ";
      $sql2 .= " WHERE id={$id}";
      $result2 = $db->query($sql2);

      if ($result2) {
        $_SESSION["message"] = "Form updated.";
        redirect_to("object_table.php");
      } else {
        $_SESSION["message"] = "Form update failed.";
      }
    } else {
      $message = form_errors($errors);
    }
  }
?>

<?php include_admin_layout_template('admin_header.php') ?>
      <div id="page">
        <h2>Update Objects Weight and Calculate Cost</h2>
        <?php echo output_message($message); ?>
        <form action="update_object.php" method="post">
          <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" />
          <input type="hidden" id="price" name="price" value="<?php echo $db->find_price($_GET['material']); ?>" />
          <p>Weight: <input type="text" id="weight" name="weight" value="" /> g</p>
          <input type="hidden" name="cost" value="" id="cost"/>
          <input type="submit" name="submit" value="Update Object"/>
        </form>
      </div>
    </div>
    <div id="footer">
      Copyright <?php echo date("Y"); ?>, G Team
    </div>
    <script src="../javascript/jquery.js"></script>
    <script type="text/javascript">
      window.onsubmit = function () {
        var weight = document.getElementById('weight').value
        var price = document.getElementById('price').value
        if(weight != "") {
          var cost = (weight * price) + 0.50;
          document.getElementById('cost').value = cost;
        } else {
          return false;
        }
      };
    </script>
  </body>
</html>
