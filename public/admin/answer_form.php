<?php
  require_once("../../includes/initialize.php");
  if(!$session->is_logged_in()){ redirect_to("login.php"); }
  $message = "";
?>

<?php
  if (isset($_POST['submit'])) {
    // Process the form
    global $db;
    // Is the OS Windows or Mac or Linux
    if (strtoupper(substr(PHP_OS,0,3)=='WIN')) {
      $eol="\r\n";
    } elseif (strtoupper(substr(PHP_OS,0,3)=='MAC')) {
      $eol="\r";
    } else {
      $eol="\n";
    }
    $from_email         = "fotmailer@plymouth.ac.uk"; //from mail, it is mandatory with some hosts
    $admin_email        = "arcijs95@gmail.com"; //recipient email (most cases it is your personal email)

    $required_fields = array("date", "time");
    validate_presence($required_fields);

    if (empty($errors)) {
      $id = $db->escape_value($_POST["id"]);
      $name = $db->escape_value($_POST["name"]);
      $mail = $db->escape_value($_POST["mail"]);
      $print_date = $db->escape_value($_POST["date"]);
      $print_time = $db->escape_value($_POST["time"]);
      $instructions = $db->escape_value($_POST["instructions"]);
      $status = $db->escape_value($_POST["status"]);

      $student_mail = $mail;
      $student_name = $name;
      $admin_name = $_SESSION['user_name'];
      $subject = "3D Printer Request Status Update";

      $success_message = "Thank you ". $student_name . " your print request has been approved and will be available for collecting after the given date and time : " . $print_date ." " . $print_time . ", at the moment we want to let you know that: ".$instructions.". but For more information you can contact us through email or find us on the 3rd floor of Smeaton building." . $eol . $eol . "(Do not reply to fotmailer@plymouth.ac.uk)" . $eol .$eol;
      $success_html = "<html><body>Thank you ". $student_name . " your print request has been approved and will be available for collecting after the given date and time : " . $print_date ." " . $print_time . ", at the moment we want to let you know that: ".$instructions.". but For more information you can contact us through email or find us on the 3rd floor of Smeaton building. </br></br> (Do not reply to fotmailer@plymouth.ac.uk) </br></br> </body></html>";

      $fail_message = "Thank you ". $student_name . " for your request but unfortunately we are unable to print out your object due: ".$instructions.". For more information you can contact us through email or find us on the 3rd floor of Smeaton building." . $eol . $eol . "(Do not reply to fotmailer@plymouth.ac.uk)" . $eol .$eol;
      $fail_html = "<html><body>Thank you ". $student_name . " for your request but unfortunately we are unable to print out your object due to: ".$instructions.". For more information you can contact us through email or find us on the 3rd floor of Smeaton building. </br></br> (Do not reply to fotmailer@plymouth.ac.uk) </br></br> </body></html>";

      $meeting_message = "Thank you ". $student_name . " your print request has been reviewed by our technician team and we have found a few flaws that we would like to discuss with you in person at the given date and time : " . $print_date ." - " . $print_time . " in Smeaton 303, but at the moment we want to let you know that: ".$instructions.". For more information you can contact us through email or find us on the 3rd floor of Smeaton building." . $eol . $eol . "(Do not reply to fotmailer@plymouth.ac.uk)" . $eol .$eol;
      $meeting_html = "<html><body>Thank you ". $student_name . " your print request has been reviewed by our technician team and we have found a few flaws that we would like to discuss with you in person at the given date and time : " . $print_date ." - " . $print_time . " in Smeaton 303, but at the moment we want to let you know that: ".$instructions.". For more information you can contact us through email or find us on the 3rd floor of Smeaton building. </br></br> (Do not reply to fotmailer@plymouth.ac.uk) </br></br> </body></html>";

      $sql = "UPDATE forms ";
      $sql .= " SET print_date='{$print_date}', ";
      $sql .= " print_time='{$print_time}', ";
      $sql .= " instructions='{$instructions}', ";
      $sql .= " status='{$status}', ";
      $sql .= " updated_by='{$admin_name}' ";
      $sql .= " WHERE id={$id}";
      $result = $db->query($sql);

      $host = "smtp.office365.com";
      $port = "587";
      $username = "fotmailer@plymouth.ac.uk";
      $password = "Generic.2810";

      $headers = array ('From' => $from_email,
        'To' => $student_email,
        'Subject' => $subject);

      $smtp = Mail::factory('smtp',
        array ('host' => $host,
          'port' => $port,
          'auth' => true,
          'username' => $username,
          'password' => $password));

      if ($result == true && $status === "Approved") {
        $mail = new Mail_mime($eol);
        $mail->setTXTBody($success_message);
        $mail->setHTMLBody($success_html);
        $body = $mail->get();
        $headers = $mail->headers($headers);
        $sentMail = $smtp->send($student_mail, $headers, $body);
      } else if ($result == true && $status === "Rejected") {
        $mail = new Mail_mime($eol);
        $mail->setTXTBody($fail_message);
        $mail->setHTMLBody($fail_html);
        $body = $mail->get();
        $headers = $mail->headers($headers);
        $sentMail = $smtp->send($student_mail, $headers, $body);
      } else if (($result == true && $status === "Meeting")) {
        $mail = new Mail_mime($eol);
        $mail->setTXTBody($meeting_message);
        $mail->setHTMLBody($meeting_html);
        $body = $mail->get();
        $headers = $mail->headers($headers);
        $sentMail = $smtp->send($student_mail, $headers, $body);
      }

      if ($sentMail) {
        $_SESSION["message"] = "Form updated.";
        redirect_to("thank_you.php");
      } else {
        $_SESSION["message"] = "Form update failed.";
      }
    } else {
      $message = form_errors($errors);
    }
  }
?>

<?php include_admin_layout_template('admin_header.php') ?>
      <div id="page">
        <h2>Approve, Reject or Invite for Meeting the Request</h2></br>
        <?php echo output_message($message); ?>
        <form id="answ_form" action="answer_form.php" method="post">
          <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" />
          <input type="hidden" name="mail" value="<?php echo $_GET['mail']; ?>" />
          <input type="hidden" name="name" value="<?php echo $_GET['name']; ?>" />
          <p>Request Status: <select name="status">
                        <option value="Approved">Approved</option>
                        <option value="Rejected">Rejected</option>
                        <option value="Meeting">Invited For Meeting</option>
                      </select></p>
          <p>Comments: </p>
          <textarea name="instructions" rows="4" cols="90" form="answ_form">Please enter any additional information here..</textarea>
          <p>Date: <input type="date" name="date" value="" /> *if rejected please provide todays date</p>
          <p>Time: <input type="time" name="time" value="" /> *if rejected please provide the current time</p>
          <input type="submit" name="submit" value="Submit" />
        </form>
      </div>
    </div>
    <div id="footer">
      Copyright <?php echo date("Y"); ?>, G Team
    </div>
  </body>
</html>
