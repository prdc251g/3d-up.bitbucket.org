<?php
  require_once("../../includes/initialize.php");
  if(!$session->is_logged_in()){ redirect_to("login.php"); }

  if(isset($_GET['student_message'])) {
    $student_message = $_GET['student_message'];
  } else {
    $student_message = "";
  }

  if(isset($_GET['module_message'])) {
    $module_message = $_GET['module_message'];
  } else {
    $module_message = "";
  }

?>

<?php include_admin_layout_template('admin_header.php') ?>
      <div id="page">
        <form action="student_invoice.php" method="POST">
          <h4>Enter Student ID to download the students invoice</h4>
          <?php echo output_message("<span style=\"color:red\">".$student_message."</span>"); ?>
          <p>Student ID: <input type="text" name="student_id" value="" /></p>
          <p><input type="submit" name="submit" value="Download" /></p>
        </form>
      </br></br></br>
      <form action="module_invoice.php" method="POST">
        <h4>Enter Module Code to download the modules invoice</h4>
        <?php echo output_message("<span style=\"color:red\">".$module_message."</span>"); ?>
        <p>Module Code: <input type="text" name="module_code" value="" /></p>
        <p><input type="submit" name="submit" value="Download" /></p>
      </form>
      </div>
    </br></br>
    </div>

<?php include_admin_layout_template('footer.php') ?>
