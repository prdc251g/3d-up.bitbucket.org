<?php
  require_once("../../includes/initialize.php");
  if(!$session->is_logged_in()){ redirect_to("login.php"); }
  $message = "";
?>

<?php
  if (isset($_POST['submit'])) {
    // Process the form
    global $db;

    $required_fields = array("name", "tolerance", "dim_x", "dim_z", "dim_y");
    validate_presence($required_fields);

    $fields_with_max_lengths = array("name" => 100, "dim_x" => 5, "dim_z" => 5, "dim_y" => 5);
    validate_max_lengths($fields_with_max_lengths);

    if (empty($errors)) {
      $id = $db->escape_value($_POST["id"]);
      $name = $db->escape_value($_POST["name"]);
      $dim_x = $db->escape_value($_POST["dim_x"]);
      $dim_z = $db->escape_value($_POST["dim_z"]);
      $dim_y = $db->escape_value($_POST["dim_y"]);
      $tolerance = $db->escape_value($_POST["tolerance"]);

      $sql = "UPDATE printers ";
      $sql .= " SET name='{$name}', ";
      $sql .= " dim_x={$dim_x}, ";
      $sql .= " dim_y={$dim_y}, ";
      $sql .= " dim_z={$dim_z}, ";
      $sql .= " tolerance='{$tolrenace}' ";
      $sql .= " WHERE id={$id}";
      $result = $db->query($sql);

      if ($result) {
        redirect_to("printer_table.php");
      } else {
        $message = "<span style=\"color:red\">Printer creation failed. Please make sure that the dimension fields are numerals only.</span>";
      }
    } else {
      $message = form_errors($errors);
    }
  }
?>

<?php include_admin_layout_template('admin_header.php') ?>
      <div id="page">
        <h2>Update Printer</h2>
        <?php echo output_message($message); ?>
        <form action="update_printer.php" method="post">
          <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" />
          <p>Printer Name: <input type="text" name="name" value="" /> </p>
          <p>Tolerance +/-: <input type="text" name="tolerance" value="" /> </p>
          <p>Max X Value: <input type="text" name="dim_x" value="" /> </p>
          <p>Max Y Value: <input type="text" name="dim_y" value="" /> </p>
          <p>Max Z Value: <input type="text" name="dim_z" value="" /> </p>
          <input type="submit" name="submit" value="Update Printer" />
        </form>
      </div>
    </div>
    <div id="footer">
      Copyright <?php echo date("Y"); ?>, G Team
    </div>
  </body>
</html>
