<?php
  require_once("../../includes/initialize.php");

  if($session->is_logged_in()){
    redirect_to("index.php");
  }

  if(isset($_POST['submit'])){
    global $db;
    $username = $db->escape_value($_POST['username']);
    $password = $db->escape_value($_POST['password']);

    $ad = "uopnet.plymouth.ac.uk";

    //$ad = "uopnet.plymouth.ac.uk,636";
    //$ad = ldap_connect("uopnet.plymouth.ac.uk:636");
    $connect = ldap_connect($ad);

    //Set some variables
    ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);
    //Bind to the ldap directory
    $bd = ldap_bind($connect, $username . "@uopnet.plymouth.ac.uk", $password);
    //or die("Unable to authenticate username - please check username and password are correct")
    //Search the directory
    $result = ldap_search($connect, "DC=uopnet,DC=plymouth,DC=ac,DC=uk", "(CN=" . $username . ")");

       if ($result == NULL) {
         // username or password was not found in database
         $message = "Username/Password combination incorrect";
         $password = "";
        } else {
        //Create result set
        $entries = ldap_get_entries($connect, $result);

      	for ($i=0; $i < $entries["count"]; $i++) {

          if (isset($entries[$i]["middlename"][0])) {
          	$firstName = $entries[$i]["givenname"][0]." ".$entries[$i]["middlename"][0];
          } else {
          	$firstName = $entries[$i]["givenname"][0];
          }
      		$lastName = $entries[$i]["sn"][0];
          $fullName = $firstName . " " . $lastName;
      		$email = $entries[$i]["mail"][0];
          $school = $entries[$i]["physicaldeliveryofficename"][0];
          $employeeID = $entries[$i]["employeeid"][0];
          $role = $entries[$i]["msexchextensionattribute18"][0];

          $entry = $session->admin_login($fullName, $email, $school, $employeeID, $role);
          if($entry) {
            redirect_to("index.php");
          } else {
            $message = "Unfortunately the 3D printing service is only available for final year students of the School of Computing, Electronics and Mathematics";
          }
        }
      }
       //never forget to unbind!
       ldap_unbind($connect);
  } else {
    $username = "";
    $password = "";
    $message = "";
  }
?>

<html>
  <head>
    <title>3D Print Submission System: Admin</title>
    <link href="../stylesheets/style.css" media="all" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <div id="header">
      <div id="himg"><img src="../assets/plym2d.png"></div>
      <div id="hh1"><h1>3D Print Submission System: Admin</h1></div>
    </div>
    <div id="main">
      <div id="navigation">
        <div id="nav">
        </div>
        <div id="line"></div>
      </div>
      <div id="page">
        <h2>Admin Login</h2></br>
        <?php echo output_message($message); ?>
        <form action="login.php" method="post" >
          <table>
            <tr>
              <td>Username:</td>
              <td><input type="text" name="username" maxlength="30" value="<?php echo htmlentities($username); ?>" /></td>
            </tr>
            <tr>
              <td>Password:</td>
              <td><input type="password" name="password" maxlength="30" value="<?php echo htmlentities($password); ?>" /></td>
            </tr>
            <tr>
              <td colspan="2">
                <input type="submit" name="submit" value="Login" />
              </td>
            </tr>
          </table>
        </form>
        <br/>
        <a href="../login.php">User Login</a>
      </div>

<?php include_admin_layout_template('footer.php') ?>
