<?php
  require_once("../../includes/initialize.php");
  if(!$session->is_logged_in()){
    redirect_to("login.php");
  }
  if(isset($_GET['d'])){
    $d = $_GET['d'];
    $oid = $_GET['id'];
  } else {
    $d = "";
  }

  if($d == "") {
    $message = "";
  } else {
    $message = "Colour with ID=".$oid." has been successfully deleted!";
  }
?>

<?php
  if (isset($_POST['submit'])) {
    // Process the form
    global $db;

    $required_fields = array("name");
    validate_presence($required_fields);

    $fields_with_max_lengths = array("name" => 20);
    validate_max_lengths($fields_with_max_lengths);

    if (empty($errors)) {
      $name = $db->escape_value($_POST["name"]);

      $sql = "INSERT INTO colours (";
      $sql .= " name";
      $sql .= ") VALUES (";
      $sql .= " '{$name}'";
      $sql .= ")";
      $result = $db->query($sql);

      if ($result) {
        $_SESSION["message"] = "Colour created.";
        redirect_to("colour_table.php");
      } else {
        $_SESSION["message"] = "Colour creation failed.";
      }
    } else {
      $message = form_errors($errors);
    }
  }
?>

<?php include_admin_layout_template('admin_header.php') ?>
      <div id="page">
        <h2>Add a new Colour</h2>
        <?php echo output_message($message); ?>
        <form action="colour_table.php" method="post">
          <p>Colour: <input type="text" name="name" value="" /> </p>
          <input type="submit" name="submit" value="Add Colour" />
        </form>
      </br></br>
        <h2>Colour Table</h2><br/>
        <table width="100%" border="1" cellpadding="1" cellspacing="1">
          <tr id="sticky">
            <th>ID</th>
            <th>Name</th>
            <th>Edit</th>
          </tr>
          <?php
            global $db;
            $records = $db->query("SELECT * FROM colours");
            while($object = mysqli_fetch_assoc($records)) {
              echo "<tr>";
              echo "<td>".$object['id']."</td>";
              echo "<td>".$object['name']."</td>";
              echo "<td><a href=\"update_colour.php?id=".$object['id']."\">
                      <img src=\"../assets/edit.png\" alt=\"Edit button\" style=\"width:30px;height:30px;border:0;\">
                    </a><a href=\"delete_record.php?id=".$object['id']."&table=colours&link=colour_table\">
                      <img src=\"../assets/trash.png\" alt=\"Delete button\" style=\"width:30px;height:30px;border:0;\">
                    </a></td>";
              echo "</tr>";
            }
          ?>
        </table>
      </div>
    </div>
    <div id="footer">
      Copyright <?php echo date("Y"); ?>, G Team
    </div>
  </body>
</html>
