<?php
  require_once("../../includes/initialize.php");
  if(!$session->is_logged_in()){
    redirect_to("login.php");
  }
  if(isset($_GET['d'])){
    $d = $_GET['d'];
    $oid = $_GET['id'];
  } else {
    $d = "";
  }
  if($d == "") {
    $message = "";
  } else {
    $message = "<span style=\"color:red\">Printer with ID=".$oid." has been successfully deleted!</span>";
  }
?>

<?php
  if (isset($_POST['submit'])) {
    // Process the form
    global $db;

    $required_fields = array("name", "tolerance", "dim_x", "dim_z", "dim_y");
    validate_presence($required_fields);

    $fields_with_max_lengths = array("name" => 100, "dim_x" => 5, "dim_z" => 5, "dim_y" => 5);
    validate_max_lengths($fields_with_max_lengths);

    if (empty($errors)) {
      $name = $db->escape_value($_POST["name"]);
      $dim_x = $db->escape_value($_POST["dim_x"]);
      $dim_z = $db->escape_value($_POST["dim_z"]);
      $dim_y = $db->escape_value($_POST["dim_y"]);
      $tolerance = $db->escape_value($_POST["tolerance"]);

      $sql = "INSERT INTO printers (";
      $sql .= " name, tolerance, dim_x, dim_y, dim_z";
      $sql .= ") VALUES (";
      $sql .= " '{$name}', '{$tolerance}', {$dim_x}, {$dim_y}, {$dim_z}";
      $sql .= ")";
      $result = $db->query($sql);

      if ($result) {
        redirect_to("printer_table.php");
      } else {
        $message = "<span style=\"color:red\">Printer creation failed. Please make sure that the dimension fields are numerals only.</span>";
      }
    } else {
      $message = form_errors($errors);
    }
  }
?>

<?php include_admin_layout_template('admin_header.php') ?>
      <div id="page">
        <h2>Add a new Printer</h2>
        <?php echo output_message($message); ?>
        <form action="printer_table.php" method="post">
          <p>Printer Name: <input type="text" name="name" value="" /> </p>
          <p>Tolerance +/-: <input type="text" name="tolerance" value="" /> </p>
          <p>Max X Value: <input type="text" name="dim_x" value="" /> </p>
          <p>Max Y Value: <input type="text" name="dim_y" value="" /> </p>
          <p>Max Z Value: <input type="text" name="dim_z" value="" /> </p>
          <input type="submit" name="submit" value="Add Printer" />
        </form>
      </br></br>
      <h2>Printer Table</h2><br/>
        <table width="100%" border="1" cellpadding="1" cellspacing="1">
          <tr id="sticky">
            <th>ID</th>
            <th>Printer Name</th>
            <th>Tolerance +/-</th>
            <th>Max Size Dimension X</th>
            <th>Max Size Dimension Y</th>
            <th>Max Size Dimension Z</th>
            <th>Edit</th>
          </tr>
          <?php
            global $db;
            $records = $db->query("SELECT * FROM printers");
            while($object = mysqli_fetch_assoc($records)) {
              echo "<tr>";
              echo "<td>".$object['id']."</td>";
              echo "<td>".$object['name']."</td>";
              echo "<td>".$object['tolerance']."</td>";
              echo "<td>".$object['dim_x']."</td>";
              echo "<td>".$object['dim_y']."</td>";
              echo "<td>".$object['dim_z']."</td>";
              echo "<td><a href=\"update_printer.php?id=".$object['id']."\">
                      <img src=\"../assets/edit.png\" alt=\"Edit button\" style=\"width:30px;height:30px;border:0;\">
                    </a>
                        <a href=\"delete_record.php?id=".$object['id']."&table=printers&link=printer_table\">
                      <img src=\"../assets/trash.png\" alt=\"Delte button\" style=\"width:30px;height:30px;border:0;\">
                    </a></td>";
              echo "</tr>";
            }
          ?>
        </table>
      </div>
    </div>
    <div id="footer">
      Copyright <?php echo date("Y"); ?>, G Team
    </div>
  </body>
</html>
