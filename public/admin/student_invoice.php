<?php
  require_once("../../includes/initialize.php");
  global $db;

  if(isset($_POST['submit'])) {

    $required_fields = array("student_id");
    validate_presence($required_fields);

    if (empty($errors)) {

      $student_id = $db->escape_value($_POST["student_id"]);

      $userInfo = $db->query("SELECT module_code, user_id, card_number, full_name, email FROM forms WHERE user_id={$student_id} LIMIT 1");
      $records = $db->query("SELECT id, material, quality, cost FROM objects WHERE user_id={$student_id}");

      $student = mysqli_fetch_assoc($userInfo);

      if ($student['user_id'] != "") {
        $cardNo = "Card Number: " ."\t". $student['card_number'];
        $studentID = "ID Number: " ."\t". $student['user_id'];
        $fullName = "Full Name: " ."\t". $student['full_name'];
        $email = "E-mail Address: " ."\t". $student['email'];
        $moduleCode = "Module Code: " ."\t". $student['module_code'];

        $columnNames = "Submissions ID" . "\t" . "Material" . "\t" . "Quality" . "\t" . "Cost" . "\t";
        $totalCost = "" . "\t" . "" . "\t" . "Total Cost" . "\t" . "" . "\t";

        $setData = '';

        while ($rec = mysqli_fetch_row($records)) {
            $rowData = '';
            foreach ($rec as $value) {
                $value = '"' . $value . '"' . "\t";
                $rowData .= $value;
            }
            $setData .= trim($rowData) . "\n";
        }

        header("Content-type: application/xls");
        header("Content-Disposition: attachment; filename=".$student['full_name']."-invoice.xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo $cardNo . "\n" . $studentID . "\n" . $fullName . "\n" . $email . "\n" . $moduleCode . "\n \n" . $columnNames . "\n" . $setData . "\n" . $totalCost . "\n";
      } else {
        redirect_to("invoice.php?student_message=The student ID number entered does not exist");
      }
    } else {
      redirect_to("invoice.php?student_message=Student ID fieldname can't be blank");
    }
  }
?>
