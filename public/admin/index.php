<?php
  require_once("../../includes/initialize.php");
  require_once('calendar/dbConnection.php');
  if(!$session->is_logged_in()){
    redirect_to("login.php");
  }

  $sql = "SELECT id, title, start, end, color FROM events ";

  $req = $dbConnection->prepare($sql);
  $req->execute();

  $events = $req->fetchAll();
?>

<html>
  <head>
    <title>3D Print Facility: Admin</title>
    <link href="../stylesheets/bootstrap.min.css" rel="stylesheet" />
    <link href='../stylesheets/fullcalendars.css' rel='stylesheet' />
    <link href="../stylesheets/style.css" media="all" rel="stylesheet" type="text/css" />

  </head>
  <body>
    <div id="header">
      <div id="himg"><img src="../assets/plym2d.png"></div>
      <div id="hh1"><h1>3D Print Submission System: Admin</h1></div>
    </div>
    <div id="main">
      <div id="navigation">
    		<div id="nav">
    		<h4>Form Actions</h4>
    		<ul>
    			<li><a href="pending_forms.php">Pending Forms</a></li>
    			<li><a href="meeting_forms.php">Meeting Forms</a></li>
    			<li><a href="form_table.php">All Forms</a></li>
    		</ul>

    		<h4>Table Actions</h4>
    		<ul>
    			<li><a href="material_table.php">Material Records</a></li>
          <li><a href="quality_table.php">Quality Records</a></li>
          <li><a href="colour_table.php">Colour Records</a></li>
    			<li><a href="printer_table.php">Printer Records</a></li>
    			<li><a href="object_table.php">Object Records</a></li>
    		</ul>

    		<h4>Main</h4>
    		<ul>
          <li><a href="invoice.php">Export Invoice</a></li>
    			<li><a href="index.php">Show Main</a></li>
    			<li><a href="logout.php">Log Out</a>
    		</ul>
    		</div>
    		<div id="line"></div>
      </div>
      <div id="page">
        <div class="containers">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3>3D Print Calendar</h3>
                    <p class="lead"></p>
                    <div id="calendar" class="col-centered">
                    </div>
                </div>

            </div>
      		<!-- Modal -->
      		<div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      		  <div class="modal-dialog" role="document">
      			<div class="modal-content">
      			<form class="form-horizontal" method="POST" action="calendar/addEvent.php">

      			  <div class="modal-header">
      				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      				<h4 class="modal-title" id="myModalLabel">Add Event</h4>
      			  </div>
      			  <div class="modal-body">

      				  <div class="form-group">
      					<label for="title" class="col-sm-2 control-label">Name</label>
      					<div class="col-sm-10">
      					  <input type="text" name="title" class="form-control" id="title" placeholder="Name">
      					</div>
      				  </div>
      				  <div class="form-group">
      					<label for="color" class="col-sm-2 control-label">Printer</label>
      					<div class="col-sm-10">
      					  <select name="color" class="form-control" id="color">
      						  <option value="">Choose</option>
      						  <option style="color:#0071C5;" value="#0071C5"> Neptune </option>
      						  <option style="color:#40E0D0;" value="#40E0D0"> Uranus </option>
      						  <option style="color:#FF0000;" value="#FF0000"> Mars </option>
      						  <option style="color:#FACD82;" value="#FACD82"> Saturn </option>
      						  <option style="color:#838383;" value="#838383"> Mercury </option>
      						  <option style="color:#C99039;" value="#C99039"> Jupiter </option>
      						</select>
      					</div>
      				  </div>
      				  <div class="form-group">
      					<label for="start" class="col-sm-2 control-label">Start date</label>
      					<div class="col-sm-10">
      					  <input type="text" name="start" class="form-control" id="start">
      					</div>
      				  </div>
      				  <div class="form-group">
      					<label for="end" class="col-sm-2 control-label">End date</label>
      					<div class="col-sm-10">
      					  <input type="text" name="end" class="form-control" id="end">
      					</div>
      				  </div>

      			  </div>
      			  <div class="modal-footer">
      				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      				<button type="submit" class="btn btn-primary">Save changes</button>
      			  </div>
      			</form>
      			</div>
      		  </div>
      		</div>
      		<!-- Modal -->
      		<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    		  <div class="modal-dialog" role="document">
    			<div class="modal-content">
    			<form class="form-horizontal" method="POST" action="calendar/editEventTitle.php">
    			  <div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    				<h4 class="modal-title" id="myModalLabel">Edit Event</h4>
    			  </div>
    			  <div class="modal-body">

    				  <div class="form-group">
    					<label for="title" class="col-sm-2 control-label">Name</label>
    					<div class="col-sm-10">
    					  <input type="text" name="title" class="form-control" id="title" placeholder="Name">
    					</div>
    				  </div>
    				  <div class="form-group">
    					<label for="color" class="col-sm-2 control-label">Printer</label>
    					<div class="col-sm-10">
    					  <select name="color" class="form-control" id="color">
    						  <option value="">Choose</option>
                  <option style="color:#0071C5;" value="#0071C5"> Neptune </option>
                  <option style="color:#40E0D0;" value="#40E0D0"> Uranus </option>
                  <option style="color:#FF0000;" value="#FF0000"> Mars </option>
                  <option style="color:#FACD82;" value="#FACD82"> Saturn </option>
                  <option style="color:#838383;" value="#838383"> Mercury </option>
                  <option style="color:#C99039;" value="#C99039"> Jupiter </option>

    						</select>
    					</div>
    				  </div>
    				    <div class="form-group">
    						<div class="col-sm-offset-2 col-sm-10">
    						  <div class="checkbox">
    							<label class="text-danger"><input type="checkbox"  name="delete"> Delete event</label>
    						  </div>
    						</div>
    					</div>

    				  <input type="hidden" name="id" class="form-control" id="id">


    			  </div>
    			  <div class="modal-footer">
    				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    				<button type="submit" class="btn btn-primary">Save changes</button>
    			  </div>
    			</form>
    			</div>
    		  </div>
    		</div>
        </div>
        <div class="pendings">
          <h3>Pending Forms</h3></br>
          <form id="search" action="index.php" method="get">
            <p>Search by: <select name="search_option">
                          <?php
                            $search_options = ["Object ID" => "id", "Timestamp" => "time_stamp", "Card Number" => "card_number", "Full Name" => "full_name", "Student ID" => "user_id", "E-Mail" => "email", "Module Code" => "module_code", "Comments" => "comments", "Print Date" => "print_date", "Printer" => "printer", "Instructions" => "instructions", "Cost" => "cost", "Status" => "status" ];
                            foreach ($search_options as $key => $value) {
                              echo "<option value='".$value."'>".$key."</option>";
                            }
                          ?>
                        </select>
              value: <input type="text" name="search_value" value="" />
            <input type="submit" name="submit" value="search" /></p>
          </form>
        </br>
          <table id="pending" width="36%" border="1" cellpadding="1" cellspacing="1">
            <tr id="sticky">
              <th>Object ID</th>
              <th>Timestamp</th>
              <th>Student ID</th>
              <th>Full Name</th>
              <th>Printer</th>
              <th>Status</th>
              <th>Edit</th>
            </tr>
            <?php
              global $db;
              if(isset($_GET['submit'])){
                $state = $db->escape_value($_GET['submit']);
                $option = $db->escape_value($_GET['search_option']);
                $value = $db->escape_value($_GET['search_value']);
              } else {
                $state = "";
              }

              if($state == "search") {
                $records = $db->query("SELECT * FROM forms WHERE status='pending' AND $option LIKE '%$value%'");
              } else {
                $records = $db->query("SELECT * FROM forms WHERE status='pending'");
              }
              while($object = mysqli_fetch_assoc($records)) {
                echo "<tr>";
                echo "<td>".$object['id']."</td>";
                echo "<td>".$object['time_stamp']."</td>";
                echo "<td>".$object['user_id']."</td>";
                echo "<td>".$object['full_name']."</td>";
                echo "<td>".$object['printer']."</td>";
                echo "<td>".$object['status']."</td>";
                echo "<td><a href=\"answer_form.php?id=".$object['id']."&name=".$object['full_name']."&mail=".$object['email']."\">
                        <img src=\"../assets/edit.png\" alt=\"Edit button\" style=\"width:30px;height:30px;border:0;\">
                      </a></td>";
                echo "</tr>";
              }
            ?>
          </table>
        </div>
    </div>
    </br></br>
    <!-- /.container -->
    <script src="../javascript/jquery.js"></script>
    <script src="../javascript/bootstrap.min.js"></script>
    <script src='../javascript/moment.min.js'></script>
    <script src='../javascript/fullcalendar.min.js'></script>
    <script>
      $(document).ready(function() {

        $('#calendar').fullCalendar({
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
          },
          defaultDate: '<?php echo date("Y-m-d"); ?>',
          defaultView: 'basicWeek',
          editable: true,
          eventLimit: true, // allow "more" link when too many events
          selectable: true,
          selectHelper: true,
          select: function(start, end) {

            $('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
            $('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
            $('#ModalAdd').modal('show');
          },
          eventRender: function(event, element) {
            element.bind('dblclick', function() {
              $('#ModalEdit #id').val(event.id);
              $('#ModalEdit #title').val(event.title);
              $('#ModalEdit #color').val(event.color);
              $('#ModalEdit').modal('show');
            });
          },
          eventDrop: function(event, delta, revertFunc) { // si changement de position

            edit(event);

          },
          eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur

            edit(event);

          },
          events: [
          <?php foreach($events as $event):

            $start = explode(" ", $event['start']);
            $end = explode(" ", $event['end']);
            if($start[1] == '00:00:00'){
              $start = $start[0];
            }else{
              $start = $event['start'];
            }
            if($end[1] == '00:00:00'){
              $end = $end[0];
            }else{
              $end = $event['end'];
            }
          ?>
            {
              id: '<?php echo $event['id']; ?>',
              title: '<?php echo $event['title']; ?>',
              start: '<?php echo $start; ?>',
              end: '<?php echo $end; ?>',
              color: '<?php echo $event['color']; ?>',
            },
          <?php endforeach; ?>
          ]
        });

        function edit(event){
          start = event.start.format('YYYY-MM-DD HH:mm:ss');
          if(event.end){
            end = event.end.format('YYYY-MM-DD HH:mm:ss');
          }else{
            end = start;
          }

          id =  event.id;

          Event = [];
          Event[0] = id;
          Event[1] = start;
          Event[2] = end;

          $.ajax({
           url: 'calendar/editEventDate.php',
           type: "POST",
           data: {Event:Event},
           success: function(rep) {
              if(rep == 'OK'){
                alert('Saved');
              }else{
                alert('Could not be saved. try again.');
              }
            }
          });
        }

      });
    </script>
<?php include_admin_layout_template('footer.php') ?>
