<?php
  require_once("../../includes/initialize.php");
  if(!$session->is_logged_in()){ redirect_to("login.php"); }
  $message = "";
?>

<?php
  if (isset($_POST['submit'])) {
    // Process the form
    global $db;

    if (empty($errors)) {
      $id = $db->escape_value($_POST["id"]);
      $name = $db->escape_value($_POST["name"]);

      $sql = "UPDATE materials ";
      $sql .= " SET name='{$name}' ";
      $sql .= " WHERE id={$id}";
      $result = $db->query($sql);

      if ($result) {
        redirect_to("material_table.php");
      } else {
        $message = "<span style=\"color:red\">Material creation failed. Please make sure that the cost field is numerals only.</span>";
      }
    }
  }
?>

<?php include_admin_layout_template('admin_header.php') ?>
      <div id="page">
        <h2>Update Material</h2>
        <?php echo output_message($message); ?>

        <form action="update_material.php" method="post">
          <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" />
          <p>Material Name: <input type="text" name="name" value="" /> </p>
          <input type="submit" name="submit" value="Update Material" />
        </form>
      </div>
    </div>
    <div id="footer">
      Copyright <?php echo date("Y"); ?>, G Team
    </div>
  </body>
</html>
