<?php
  require_once("../../includes/initialize.php");
  if(!$session->is_logged_in()){ redirect_to("login.php"); }
  $message = "";
?>

<?php
  if (isset($_POST['submit'])) {
    // Process the form
    global $db;

    if (empty($errors)) {
      $id = $db->escape_value($_POST["id"]);
      $quality = $db->escape_value($_POST["quality"]);
      $layer_height = $db->escape_value($_POST["layer"]);
      $infill_density = $db->escape_value($_POST["infill"]);
      $wall_thickness = $db->escape_value($_POST["wall"]);

      $sql = "UPDATE quality ";
      $sql .= " SET quality='{$quality}', ";
      $sql .= " layer_height='{$layer_height}', ";
      $sql .= " infill_density='{$infill_density}', ";
      $sql .= " wall_thickness='{$wall_thickness}' ";
      $sql .= " WHERE id={$id}";
      $result = $db->query($sql);

      if ($result) {
        $_SESSION["message"] = "Form updated.";
        redirect_to("quality_table.php");
      } else {
        $_SESSION["message"] = "Form update failed.";
      }
    }
  }
?>

<?php include_admin_layout_template('admin_header.php') ?>
      <div id="page">
        <h2>Update Quality</h2>
        <?php echo output_message($message); ?>
        <form action="update_quality.php" method="post">
          <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" />
          <p>Quality level: <input type="text" name="quality" value="" /> </p>
          <p>Layer height: <input type="text" name="layer" value="" /> </p>
          <p>Infill density: <input type="text" name="infill" value="" /> </p>
          <p>Wall thickness: <input type="text" name="wall" value="" /> </p>
          <input type="submit" name="submit" value="Update Quality" />
        </form>
      </div>
    </div>
    <div id="footer">
      Copyright <?php echo date("Y"); ?>, G Team
    </div>
  </body>
</html>
