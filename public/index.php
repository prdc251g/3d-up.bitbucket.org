<?php
  require_once("../includes/initialize.php");
  if(!$session->is_logged_in()){
    redirect_to("login.php");
  }
?>
<?php include_layout_template('header.php') ?>
      <div id="page">
        <h2>Welcome to the 3D print studio <?php echo $_SESSION['user_name']; ?>!</h2></br>
        <h4>A short brief about the site and what to expect.</h4>
        <p>For the 2016-17 academic year, a new module was introduced that required rapid prototyping machines to manufacture robotic parts and designs. Two 3D printers were purchased for the module and provided students with open access to the printers during normal lab hours.</p>
        <p>After the module ends in the first semester, it has been decided that the printers will be opened up to <span style="color:red;">final year and masters students only</span> for use with academic projects and thesis.</p>
        <p>The ‘3D print submission system’ will handle files and submission logging through a cloud-based approach, using online forms to log and store files whilst emails will be used for student-technician communication.</p>
        <p><span style="color:red;">The cost of printing</span> will be measured by the weight of the printed part (including any support structure) measured in grams. The cost for a 1Kg roll of the chosen PLA filament 1 comes out at £0.028 per gram. An additional £0.50 will be applied per submission which will be used for tooling costs and upkeep of the printers.</p>
        <p>When submitting the file containing the 3D object designs please <span style="color:red;">do not go over 7.5 MB and have the files zipped up</span>. We only accept .STL file format. Failure to follow these file specifications will result in a <span style="color:red;">rejected request</span>.</p>
        <p>After your files have been submitted a member of our technical team will have to check it and if they require changes to be made to the 3D design then a face to face meeting will be arranged to talk through the details in case any mistakes have been made or we require you to adjust the file for it to be suitable for our printers. Otherwise once the request has been approved an email with a date and time will be sent out regarding the infomation on when will the item be ready for pick up.</p>
        <p>Each printer has a printing tolerance associated with it. <span style="color:red;">Take this into account when finalising your parts</span>!
          <ul>
            <li>Prusa i3 Mk2: +/- 0.2mm.</li>
            <li>Bq Hephestos: +/- 0.4mm.</li>
            <li>Makerbot Replicator 2x: +/- 0.4mm.</li>
            <li>By selecting 'Any Printer', the technical team will allocate a printer to use.</li>
          </ul>
          </p>
        <p>For advice on optimising parts <a href="https://www.3dhubs.com/knowledge-base/collection/basic-principles-of-3d-printing">click here</a></p>

      </div>
  </div>

<?php include_layout_template('footer.php') ?>
