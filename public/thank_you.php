<?php
  require_once("../includes/initialize.php");
  if(!$session->is_logged_in()){
    redirect_to("login.php");
  }
?>
<?php include_layout_template('header.php') ?>
    <div id="page">
      <h2>You have successfully submited your request!</h2>
      <br/>
      <p>We have sent you a confirmation email to your university email address with the details of your request form as well as a copy of the file you submited. We'll get in touch shortly.</p>
      <br/>
      <a href="request_form.php">Have Another Request?</a>
    </div>

<?php include_layout_template('footer.php') ?>
