<?php
  require_once("../includes/initialize.php");
  if(!$session->is_logged_in()){ redirect_to("login.php"); }
  $message = "";
?>

<?php
//if($_POST['submit'] && isset($_FILES['file_upload']))
if(isset($_POST['submit']))
{
  global $db;
  global $errors;
  // Is the OS Windows or Mac or Linux
  if (strtoupper(substr(PHP_OS,0,3)=='WIN')) {
    $eol="\r\n";
  } elseif (strtoupper(substr(PHP_OS,0,3)=='MAC')) {
    $eol="\r";
  } else {
    $eol="\n";
  }
  $from_email         = "fotmailer@plymouth.ac.uk"; //from mail, it is mandatory with some hosts
  $admin_email        = "arcijs95@gmail.com"; //recipient email (most cases it is your personal email)

  $required_fields = array("quantity", "dim_x", "dim_z", "dim_y");
  validate_presence($required_fields);

  $fields_with_max_lengths = array("quantity" => 3, "dim_x" => 5, "dim_z" => 5, "dim_y" => 5);
  validate_max_lengths($fields_with_max_lengths);

  if (empty($errors)) {
    //Capture POST data from HTML form and Sanitize them,
    $dim_x = $db->escape_value($_POST["dim_x"]);
    $dim_z = $db->escape_value($_POST["dim_z"]);
    $dim_y = $db->escape_value($_POST["dim_y"]);
    $quantity = $db->escape_value($_POST["quantity"]);
    $material = $db->escape_value($_POST["material"]);
    $colour = $db->escape_value($_POST["colour"]);
    $quality = $db->escape_value($_POST["quality"]);
    $comments = $db->escape_value($_POST["comments"]);
    $printer = $db->escape_value($_POST["printer"]);
    $module_code = $db->escape_value($_POST["module"]);

    $student_name = $_SESSION['user_name'];
    $student_email = $_SESSION['user_mail'];
    $student_id = $_SESSION['user_id'];
    $student_cardNo = $_SESSION['user_cardNo'];
    $subject = "3D Print Request";
    $admin_message = $student_name . " - Email address:- " . $student_email . " has requested to use the 3D printing facility!" . $eol . $eol . "(Do not reply to fotmailer@plymouth.ac.uk)" . $eol .$eol;
    $admin_html = "<html><body>".$student_name . " - Email address:- " . $student_email . " has requested to use the 3D printing facility! </br></br> (Do not reply to fotmailer@plymouth.ac.uk) </br></br> </body></html>";

    $student_message = "Thank you ". $student_name . " we have successfully received your request to use the 3D printing facility!" . $eol . $eol . "(Do not reply to fotmailer@plymouth.ac.uk)" . $eol .$eol;
    $student_html = "<html><body>Thank you ". $student_name . " we have successfully received your request to use the 3D printing facility! </br></br> (Do not reply to fotmailer@plymouth.ac.uk) </br></br> </body></html>";

    //Get uploaded file data
    $file_tmp_name    = $_FILES['file_upload']['tmp_name'];
    $file_name        = $_FILES['file_upload']['name'];
    $file_size        = $_FILES['file_upload']['size'];
    $file_type        = $_FILES['file_upload']['type'];
    $file_error       = $_FILES['file_upload']['error'];

    if($file_error > 0)
    {
      $errors['file_upload'] = "There seems to be an upload error or no file was uploaded";
      $message = form_errors($errors);
    } else {
      $sql1 = "INSERT INTO objects (";
      $sql1 .= "user_id, filename, type, size, dim_x, dim_y, dim_z, quantity, material, colour, quality, comments";
      $sql1 .= ") VALUES (";
      $sql1 .= " {$student_id}, '{$file_name}', '{$file_type}', {$file_size}, {$dim_x}, {$dim_y}, {$dim_z}, {$quantity}, '{$material}', '{$colour}', '{$quality}', '{$comments}'";
      $sql1 .= ")";
      $result1 = $db->query($sql1);

      if ($result1){
        $object_id = $db->find_last_id('objects');
        $sql2 = "INSERT INTO forms (";
        $sql2 .= "id, user_id, card_number, full_name, email, module_code, comments, printer";
        $sql2 .= ") VALUES (";
        $sql2 .= " {$object_id}, {$student_id}, {$student_cardNo}, '{$student_name}', '{$student_email}', '{$module_code}', '{$comments}', '{$printer}'";
        $sql2 .= ")";
        $result2 = $db->query($sql2);
      } else {
        $errors['sql'] = "SQL query was unsuccessful. Please make sure that the qunatity and dimension fields are numbers only and no letters or special characters!";
        $message = form_errors($errors);
      }

      if ($result1 && $result2){

        $host = "smtp.office365.com";
        $port = "587";
        $username = "fotmailer@plymouth.ac.uk";
        $password = "Generic.2810";

        $adminHeaders = array ('From' => $from_email,
          'To' => $admin_email,
          'Subject' => $subject);

        $studentHeaders = array ('From' => $from_email,
          'To' => $student_email,
          'Subject' => $subject);

        $smtp = Mail::factory('smtp',
          array ('host' => $host,
            'port' => $port,
            'auth' => true,
            'username' => $username,
            'password' => $password));

        $Amime = new Mail_mime($eol);
        $Amime->setTXTBody($admin_message);
        $Amime->setHTMLBody($admin_html);
        $Amime->addAttachment($file_tmp_name, $file_type, $file_name);
        $Abody = $Amime->get();
        $Aheaders = $Amime->headers($adminHeaders);
        $Amail = $smtp->send($admin_email, $Aheaders, $Abody);

        $Smime = new Mail_mime($crlf);
        $Smime->setTXTBody($student_message);
        $Smime->setHTMLBody($student_html);
        $Smime->addAttachment($file_tmp_name, $file_type, $file_name);
        $Sbody = $Smime->get();
        $Sheaders = $Smime->headers($studentHeaders);
        $Smail = $smtp->send($student_email, $Sheaders, $Sbody);

        if (PEAR::isError($Amail) || PEAR::isError($Smail)) {
         echo("<p>" . $Amail->getMessage() . "</p>");
         echo("<p>" . $Smail->getMessage() . "</p>");
        } else {
         redirect_to("thank_you.php");
        }

      } else {
        $errors['sql'] = "SQL query was unsuccessful. Please make sure that the qunatity and dimension fields are numbers only and no letters or special characters!";
        $message = form_errors($errors);
      }
    }
  } else {
    $message = form_errors($errors);
  }
}

?>

<?php include_layout_template('header.php') ?>
      <div id="page">
        <h2>Request Form</h2></br>
        <?php echo output_message($message); ?>
        <form id="req_form" action="request_form.php" enctype="multipart/form-data" method="POST">
          <input type="hidden" name="MAX_FILE_SIZE" value="24000000" />
          <p>Module: <select name="module">
                        <?php
                          $modules = $_SESSION['user_modules'];
                          $moduleArray = explode(" ",$modules);
                          foreach ($moduleArray as $module) {
                            $intStr = filter_var($module, FILTER_SANITIZE_NUMBER_INT);
                            $valArr = str_split($intStr);
                            if($valArr[0] == 2){
                              echo "<option value='".$module."'>".$module."</option>";
                            }
                          }
                        ?>
                      </select></p>
          <p>Printer: <select name="printer">
                        <?php
                          $sql = "SELECT name, dim_x, dim_y, dim_z, tolerance FROM printers";
                          $result = $db->query($sql);
                          while ($row = $db->fetch_array($result))
                            echo "<option value='".$row['name']."'>".$row['name']." - Max Dimensions: X: ".$row['dim_x']."mm, Y: ".$row['dim_y']."mm, Z: ".$row['dim_z']."mm, Tolerance: +/- ".$row['tolerance']."</option>";
                        ?>
                      </select></p>
          <p>Quantity: <input type="text" name="quantity" value="" /></p>
          <p>Dimension X: <input type="text" name="dim_x" value="" /> mm</p>
          <p>Dimension Y: <input type="text" name="dim_y" value="" /> mm</p>
          <p>Dimension Z: <input type="text" name="dim_z" value="" /> mm</p>
          <p>Material: <select name="material">
                        <?php
                          $sql = "SELECT id, name FROM materials";
                          $result = $db->query($sql);
                          while ($row = $db->fetch_array($result))
                            echo "<option value='".$row['name']."'>".$row['name']."</option>";
                        ?>
                      </select>
                Colour: <select name="colour">
                              <?php
                                $sql = "SELECT id, name FROM colours";
                                $result = $db->query($sql);
                                while ($row = $db->fetch_array($result))
                                  echo "<option value='".$row['name']."'>".$row['name']."</option>";
                              ?>
                            </select>
          </p>
          <p>Quality: <select id="qSelect" name="quality" onchange="myFunction(this.value)">
                        <?php
                          $sql = "SELECT id, quality, layer_height, infill_density, wall_thickness FROM quality";
                          $result = $db->query($sql);
                          while ($row = $db->fetch_array($result))
                            echo "<option value='".$row['quality']."'>".$row['quality']." - Layer Height: ".$row['layer_height'].", Infill Density: ".$row['infill_density'].", Wall Thickness:".$row['wall_thickness']."</option>";
                        ?>
                        </script>
                      </select>
          <p>File Upload: <input type="file" name="file_upload" /> <span style="color:red;">*</span>Please upload your files in .STL format if multiple files, attach them in a .zip file up to 7.5mb.</p>
          <p>Additional Instructions: </p>
          <textarea name="comments" rows="4" cols="90" form="req_form">Please enter any additional information here..</textarea>
          <input id="submitBtn" type="submit" name="submit" value="Submit" />
        </form>
      </div>
    </div>
    <div id="footer">
      Copyright <?php echo date("Y"); ?>, G Team
    </div>
    <script src="javascript/jquery.js"></script>
    <script type="text/javascript">
      window.onbeforeunload = function () {
          $("input[type=button], input[type=submit]").attr("disabled", "disabled").val("Submitting, please wait...");
    };
</script>
  </body>
</html>
